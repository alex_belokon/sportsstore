﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsStore.WebUI.Models.Cart
{
	public class CartIndexViewModel
	{
		public Core.Models.Cart Cart { get; set; }
		public string ReturnUrl { get; set; }
	}
}