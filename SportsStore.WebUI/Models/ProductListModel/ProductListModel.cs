﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsStore.WebUI.Models.ProductListModel
{
    public class ProductListModel
    {
        public Core.Models.Product[] Products { get; set; }
		public PagingInfoModel PagingInfoModel { get; set; }
		public string CurrentCategory { get; set; }
	}
}