﻿using Ninject;
using SportsStore.Business.Managers;
using SportsStore.Core.Interfaces;
using SportsStore.Core.Interfaces.Managers;
using SportsStore.Core.Interfaces.Repositories;
using SportsStore.Core.Interfaces.UnitOfWork;
using SportsStore.Data.Context;
using SportsStore.Data.EmailSettings;
using SportsStore.Data.Repository;
using SportsStore.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {

			_kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
			_kernel.Bind<IProductRepository>().To<ProductRepository>();
			_kernel.Bind<IProductManager>().To<ProductManager>();
			_kernel.Bind<ISportsStoreDbContext>().To<SportsStoreDbContext>().InSingletonScope();
			EmailSettings emailSettings = new EmailSettings();
			emailSettings.WriteAsFile = bool.Parse(ConfigurationManager.AppSettings["Email.WriteAsFile"] ?? "false");
			_kernel.Bind<IOrderManager>().To<OrderManager>().WithConstructorArgument("_emailSettings", emailSettings);

		}

        public object GetService(Type serviceType)
        {
            return _kernel.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }
    }
}