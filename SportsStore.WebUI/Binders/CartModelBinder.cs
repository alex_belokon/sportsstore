﻿using SportsStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Binders
{
	public class CartModelBinder : IModelBinder
	{
		private const string SessionKey = "Cart";
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			// get Cart object from session
			Cart cart = null;
			if (controllerContext.HttpContext.Session != null)
			{
				cart = (Cart)controllerContext.HttpContext.Session[SessionKey];
			}
			// create Cart objeсt if it isn't found in Session dict by key
			if (cart == null)
			{
				cart = new Cart();
				if (controllerContext.HttpContext.Session != null)
				{
					controllerContext.HttpContext.Session[SessionKey] = cart;
				}
			}
			return cart;
		}
	}
}