﻿using SportsStore.Core.Interfaces.Managers;
using SportsStore.Core.Models;
using SportsStore.WebUI.Models.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Controllers
{
	public class CartController : Controller
	{
		private IProductManager _productManager;
		private IOrderManager _orderManager;

		public CartController(IProductManager productManager, IOrderManager orderManager)
		{
			_productManager = productManager;
			_orderManager = orderManager;
		}

		public RedirectToRouteResult AddToCart(Cart cart, int productId, string returnUrl)
		{
			var product = _productManager.GetAllProducts().FirstOrDefault(x => x.ProductId == productId);
			if (product != null)
			{
				cart.AddItem(product, 1);
			}
			return RedirectToAction("Index", new { returnUrl });
		}

		public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
		{
			var product = _productManager.GetAllProducts().FirstOrDefault(x => x.ProductId == productId);
			if (product != null)
			{
				cart.RemoveLine(product);
			}
			return RedirectToAction("Index", new { returnUrl });
		}

		public ViewResult Index(Cart cart, string returnUrl)
		{
			var model = new CartIndexViewModel()
			{
				Cart = cart,
				ReturnUrl = returnUrl
			};
			return View(model);
		}
		public PartialViewResult Summary(Cart cart)
		{
			return PartialView(cart);
		}
		[HttpGet]
		public ViewResult Checkout()
		{
			return View(new ShippingDetails());
		}
		[HttpPost]
		public ViewResult Checkout(Cart cart, ShippingDetails shippingDetails)
		{
			if (!cart.Lines.Any())
			{
				ModelState.AddModelError("", "Sorry, your cart is empty!");
			}
			if (ModelState.IsValid)
			{
				_orderManager.ProcessOrder(cart, shippingDetails);
				cart.Clear();
				return View("Completed");
			}
			else
			{
				return View(shippingDetails);
			}
		}
	}
}