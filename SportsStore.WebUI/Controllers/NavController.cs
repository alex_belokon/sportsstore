﻿using SportsStore.Core.Interfaces.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Controllers
{
    public class NavController : Controller
    {
		private readonly IProductManager _productManager;
		public NavController(IProductManager productManager)
		{
			_productManager = productManager;
		}
		
		public PartialViewResult Menu(string category = null)
        {
			ViewBag.SelectedCategory = category ;
			var categories = _productManager.GetAllProducts()
				.Select(x => x.Category)
				.Distinct()
				.OrderBy(x => x);
			return PartialView ("FlexMenu", categories);
        }
    }
}