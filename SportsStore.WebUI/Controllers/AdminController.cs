﻿using SportsStore.Core.Interfaces.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.WebUI.Models.Product;

namespace SportsStore.WebUI.Controllers
{
	[Authorize]
	[Authorize(Roles ="admin")]
    public class AdminController : Controller
    {
		private readonly IProductManager _productManager;

		public AdminController(IProductManager productManager)
		{
			_productManager = productManager;
		}
		// GET: Admin
		public ActionResult Index()
        {
			var products = _productManager.GetAllProducts();
			return View(products);
		}
		[HttpGet]
		public ActionResult AddNewProduct()
		{
			return View("ProductDetails", new ProductModel());
		}

		[HttpPost]
		public ActionResult AddNewProduct(ProductModel product, HttpPostedFileBase image = null)
		{
			

			if (ModelState.IsValid)
			{
				if (image != null)
				{
					product.ImageMimeType = image.ContentType;
					product.ImageData = new byte[image.ContentLength];
					image.InputStream.Read(product.ImageData, 0, image.ContentLength);
				}
				_productManager.AddProduct(product.Name, product.Description, product.Category, product.Price, product.ImageMimeType, product.ImageData);
				TempData["message"] = $"{product.Name} has been saved";
				return RedirectToAction("Index");
			}
			return View("ProductDetails", product);
		}
		[HttpGet]
		public ActionResult EditProduct(int productId)
		{
			var model = GetProductModel(productId);
			return View("ProductDetails", model);
		}
		[HttpPost]
		public ActionResult EditProduct(ProductModel product, HttpPostedFileBase image = null)
		{

			if (ModelState.IsValid)
			{
				if (image != null)
				{
					product.ImageMimeType = image.ContentType;
					product.ImageData = new byte[image.ContentLength];
					image.InputStream.Read(product.ImageData, 0, image.ContentLength);
				}
				_productManager.EditProduct(product.ProductId, product.Name, product.Description, product.Category,
					product.Price, product.ImageMimeType, product.ImageData);
				TempData["message"] = $"{product.Name} has been saved";
				return RedirectToAction("Index");
			}

			return View("ProductDetails", product);
		}
		[HttpPost]
		public ActionResult DeleteProduct(ProductModel product)
		{
			var nameForDeleted = product.Name;
			_productManager.DeleteProduct(product.ProductId);
			TempData["message"] = $"{nameForDeleted} was deleted";
			return RedirectToAction("Index");
		}
		private ProductModel GetProductModel(int productId)
		{
			var product = _productManager.GetProductById(productId);
			var model = new ProductModel()
			{
				ProductId = product.ProductId,
				Description = product.Description,
				Name = product.Name,
				Price = product.Price,
				Category = product.Category,
				ImageData = product.ImageData,
				ImageMimeType = product.ImageMimeType
			};
			return model;
		}
	}
}