﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SportsStore.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(null, "admin",
                new
                {
                    controller = "Admin",
                    action = "Index"
                }
            );
            //first page with products all category
            routes.MapRoute(
				name: "First",
				url: "",
				defaults: new { controller = "Product", action = "List", category = (string)null, page = 1 }
			);
			//Custom PageNumber from all category
			routes.MapRoute(null, "Page{page}",
			 new
			 {
				 controller = "Product",
				 action = "List",
				 category = (string)null
			 },
			 new
			 { page = @"\d+" }
		 );
			//First page from current category
			routes.MapRoute(
				name: null,
				url: "{category}",
				defaults: new { controller = "Product", action = "List", page = 1 }
			);
			//current page from current category
			routes.MapRoute(null, "{category}/Page{page}",
				new
				{
					controller = "Product",
					action = "List"
				},
				new { page = @"\d+" }
			);
            
            routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new
				{
					controller = "Product",
					action = "List",
					id = UrlParameter.Optional
				}
			);
		}
    }
}
