﻿using SportsStore.WebUI.Models.ProductListModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.HtmlHelpers
{
	public static class HtmlPagingHelper
	{
		public static System.Web.Mvc.MvcHtmlString PageLinks(this HtmlHelper htmlHelper, PagingInfoModel pagingInfoModel, Func<int, string> pageUriDelegate)
		{
			var result = new StringBuilder();
			for (int i = 1; i <= pagingInfoModel.TotalPages; i++)
			{
				var tag = new TagBuilder("a");
				tag.MergeAttribute("href", pageUriDelegate(i));
				tag.InnerHtml = i.ToString();
				if (i == pagingInfoModel.CurrentPage)
				{
					tag.AddCssClass("selected");
					tag.AddCssClass("btn-primary");
				}
				tag.AddCssClass("btn btn-default");
				result.Append(tag);
			}
			return MvcHtmlString.Create(result.ToString());
		}
	}
}