﻿CREATE TABLE [dbo].[Products] (
    [ProductID]     INT             IDENTITY (1, 1) NOT NULL,
    [Name]          VARCHAR (100)   NOT NULL,
    [Description]   VARCHAR (500)   NOT NULL,
    [Category]      VARCHAR (50)    NOT NULL,
    [Price]         MONEY           NOT NULL,
    [ImageData]     VARBINARY (MAX) NULL,
    [ImageMimeType] VARCHAR (50)    NULL,
    CONSTRAINT [PK_Products_ProductID] PRIMARY KEY CLUSTERED ([ProductID] ASC)
);

