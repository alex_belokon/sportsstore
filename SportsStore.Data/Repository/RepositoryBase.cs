﻿using SportsStore.Core.Interfaces.Repositories;
using SportsStore.Core.Interfaces.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Data.Repository
{ /// <summary>
  /// Implements the base repository.
  /// </summary>
  /// <typeparam name="T">The type of Entity.</typeparam>
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase{T}"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        protected RepositoryBase(IUnitOfWork unitOfWork)
        {
            this.DataContext = unitOfWork.DbContext as DbContext;
        }

        /// <summary>
        /// Gets the data context.
        /// </summary>
        protected DbContext DataContext
        {
            get;
            private set;
        }

        /// <summary>
        /// Adds a new entity to database Set.
        /// </summary>
        /// <param name="entity">The Entity instance.</param>
        public void Insert(T entity)
        {
            this.DataContext.Set<T>().Add(entity);
        }

        /// <summary>
        /// Adds the given collection of entities into context underlying the set with
        /// each entity being put into the Added state such that it will be inserted
        /// into the database when SaveChanges is called.
        /// </summary>
        /// <param name="entities">The collection of entities to insert.</param>
        public void InsertRange(IEnumerable<T> entities)
        {
            this.DataContext.Set<T>().AddRange(entities);
        }

        /// <summary>
        /// Updates existing entity.
        /// </summary>
        /// <param name="entity">The Entity instance.</param>
        public void Update(T entity)
        {
            var entry = this.DataContext.Entry<T>(entity);

            if ((entry == null) || (entry.State == EntityState.Detached))
            {
                this.DataContext.Set<T>().Attach(entity);
                entry.State = EntityState.Modified;
            }
        }

        /// <summary>
        /// Updates the given collection of entities.
        /// </summary>
        /// <param name="entities">The collection of entities to update.</param>
        public void UpdateRange(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                this.Update(entity);
            }
        }

        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="entity">The Entity instance.</param>
        public void Delete(T entity)
        {
            this.DataContext.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Removes the given collection of entities from the context underlying the
        /// set with each entity being put into the Deleted state such that it will be
        /// deleted from the database when SaveChanges is called.
        /// </summary>
        /// <param name="entities">The collection of entities to delete.</param>
        public void DeleteRange(IEnumerable<T> entities)
        {
            this.DataContext.Set<T>().RemoveRange(entities);
        }

        /// <summary>
        /// Find entity by key.
        /// </summary>
        /// <param name="key">Primary key value.</param>
        /// <returns>The Entity instance.</returns>
        public T Find(object key)
        {
            return this.DataContext.Set<T>().Find(key);
        }

        /// <summary>
        /// Select all entities.
        /// </summary>
        /// <returns>List of entities.</returns>
        public IEnumerable<T> SelectAll()
        {
            return this.Select().ToArray();
        }
        /* In order to make it work - we need to install EntityFramework.Extended
        /// <summary>
        /// Batch Deletes the entities by filter.
        /// </summary>
        /// <param name="filter">Filter expression.</param>
        protected void BatchDelete(Expression<Func<T, bool>> filter)
        {
            EntityFramework.Extensions.BatchExtensions.Delete<T>(this.Select(filter));
        }

        /// <summary>
        /// Batch update the entities by filter.
        /// </summary>
        /// <param name="filter">Filter expression.</param>
        /// <param name="update">Update expression.</param>
        protected void BatchUpdate(Expression<Func<T, bool>> filter, Expression<Func<T, T>> update)
        {
            EntityFramework.Extensions.BatchExtensions.Update<T>(this.Select(filter), update);
        }
        */

        /// <summary>
        /// Select entities.
        /// </summary>
        /// <returns>List of entities.</returns>
        protected DbSet<T> Select()
        {
            return this.DataContext.Set<T>();
        }

        /// <summary>
        /// Select entities with filter.
        /// </summary>
        /// <param name="filter">Filter expression.</param>
        /// <returns>Collection of entities.</returns>
        protected IQueryable<T> Select(Expression<Func<T, bool>> filter)
        {
            return this.Select().Where(filter);
        }

        /// <summary>
        /// Selects entities with the related objects.
        /// </summary>
        /// <param name="paths">List of the path to be included.</param>
        /// <returns>Collection of entities.</returns>
        protected IQueryable<T> SelectIncluding(params Expression<Func<T, object>>[] paths)
        {
            IQueryable<T> query = this.Select();

            foreach (var path in paths)
            {
                query = query.Include(path);
            }

            return query;
        }

        /// <summary>
        /// Select entities with filter and related objects
        /// </summary>
        /// <param name="filter">Filter expression.</param>
        /// <param name="paths">List of the path to be included.</param>
        /// <returns>Collection of entities.</returns>
        protected IQueryable<T> SelectFilterIncluding(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] paths)
        {
            IQueryable<T> query = this.Select(filter);

            return paths.Aggregate(query, (current, path) => current.Include(path));
        }
    }
}
