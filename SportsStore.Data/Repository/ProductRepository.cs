﻿using SportsStore.Core.Interfaces.Repositories;
using SportsStore.Core.Interfaces.UnitOfWork;
using SportsStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Data.Repository
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Product Get(int id)
        {
            return this.Select(x => x.ProductId == id).FirstOrDefault();
        }
    }
}
