﻿using SportsStore.Core.Enums;
using SportsStore.Core.Interfaces;
using SportsStore.Core.Interfaces.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Data.UnitOfWork
{
    /// <summary>
    /// Implements Unit of Work.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// The database context.
        /// </summary>
        private DbContext context;

        /// <summary>
        /// The disposed flag.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// The transaction.
        /// </summary>
        private DbContextTransaction transaction = null;

        /// <summary>
        /// The transactions count.
        /// </summary>
        private int transactionsCount = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <exception cref="System.ArgumentException">Entity Database Context instance is expected as a database Context parameter.</exception>
        public UnitOfWork(ISportsStoreDbContext context)
        {
            this.context = context as DbContext;

            if (context == null)
            {
                throw new ArgumentException("Entity.DbContext instance is expected as a dbContext parameter.");
            }

            this.context.Configuration.LazyLoadingEnabled = false;
            this.context.Configuration.ProxyCreationEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 990;
            // this.context.Configuration.AutoDetectChangesEnabled = false;

            // this.context.Database.Log = txt => System.Diagnostics.Debug.WriteLine(txt);
        }

        /// <summary>
        /// Gets the data context.
        /// </summary>
        public ISportsStoreDbContext DbContext
        {
            get
            {
                return (ISportsStoreDbContext)this.context;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the opened transaction exists.
        /// </summary>
        public bool TransactionOpened
        {
            get
            {
                return this.transaction != null;
            }
        }

        /// <summary>
        /// Saves all changes within unit of work.
        /// </summary>
        /// <returns>The number of objects written to the underlying database.</returns>
        /// <exception cref="System.ApplicationException">Validation Errors collection.</exception>
        /// <exception cref="System.Exception">Updating database error.</exception>
        public int Save()
        {
            int result = 0;

            try
            {
                result = this.context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                string errorMessage = string.Empty;

                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    if (!validationResult.IsValid)
                    {
                        string entityName = validationResult.Entry.Entity.ToString();
                        errorMessage += entityName + " : ";
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            errorMessage += error.ErrorMessage + "; ";
                        }
                    }
                }

                throw new ApplicationException(errorMessage, ex);
            }
            catch (DbUpdateException updateEx)
            {
                throw new Exception("Updating database error.", updateEx);
            }

            return result;
        }

        /// <summary>
        /// Begins a transaction on the underlying store connection using the specified isolation level.
        /// </summary>
        /// <param name="transactionIsolationLevel">The transaction isolation level.</param>
        /// <exception cref="System.NotImplementedException">Multiple transactions are not implemented.</exception>
        public void BeginTransaction(TransactionIsolationLevel transactionIsolationLevel = TransactionIsolationLevel.ReadCommitted)
        {
            if (this.transaction == null)
            {
                IsolationLevel isolationLevel = IsolationLevel.Serializable;
                switch (transactionIsolationLevel)
                {
                    case TransactionIsolationLevel.ReadUncommitted:
                        isolationLevel = IsolationLevel.ReadUncommitted;
                        break;
                    case TransactionIsolationLevel.ReadCommitted:
                        isolationLevel = IsolationLevel.ReadCommitted;
                        break;
                    case TransactionIsolationLevel.RepeatableRead:
                        isolationLevel = IsolationLevel.RepeatableRead;
                        break;
                    case TransactionIsolationLevel.Serializable:
                        isolationLevel = IsolationLevel.Serializable;
                        break;
                    case TransactionIsolationLevel.Snapshot:
                        isolationLevel = IsolationLevel.Snapshot;
                        break;
                }

                this.transaction = this.context.Database.BeginTransaction(isolationLevel);
            }

            this.transactionsCount++;
        }

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        public void CommitTransaction()
        {
            if (this.transaction == null)
            {
                throw new Exception("There is no open transaction to be committed.");
            }
            else if (this.transactionsCount == 1)
            {
                if ((this.transaction.UnderlyingTransaction != null)
                    && (this.transaction.UnderlyingTransaction.Connection != null))
                {
                    this.transaction.Commit();
                }

                this.transaction.Dispose();
                this.transaction = null;
            }

            this.transactionsCount--;
        }

        /// <summary>
        /// Rollbacks the transaction.
        /// </summary>
        public void RollbackTransaction()
        {
            if (this.transaction == null)
            {
                throw new Exception("There is no open transaction to be rolled back.");
            }
            else if (this.transactionsCount == 1)
            {
                if ((this.transaction.UnderlyingTransaction != null)
                    && (this.transaction.UnderlyingTransaction.Connection != null))
                {
                    this.transaction.Rollback();
                }

                this.transaction.Dispose();
                this.transaction = null;
            }

            this.transactionsCount--;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (this.transaction != null)
                    {
                        this.transaction.Rollback();
                        this.transaction.Dispose();
                    }

                    this.context.Dispose();
                }
            }

            this.disposed = true;
        }
    }
}
