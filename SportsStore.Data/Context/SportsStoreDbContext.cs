﻿using SportsStore.Core.Interfaces;
using SportsStore.Core.Models;
using SportsStore.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Data.Context
{
    public class SportsStoreDbContext : DbContext, ISportsStoreDbContext
    {
        public SportsStoreDbContext() : base("name=SportsStoreEntities")
        {
            //this.Database.Log = txt => System.Diagnostics.Debug.WriteLine(txt);

            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded.
            //Make sure the provider assembly is available to the running application.
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public SportsStoreDbContext(string connectionStringName) : base("name=" + connectionStringName)
        {
            var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public virtual DbSet<Product> Products { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // don't create database at startup
            Database.SetInitializer<SportsStoreDbContext>(null);

            modelBuilder.Configurations.Add(new ProductMap());
        }
    }
}
