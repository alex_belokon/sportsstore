﻿using SportsStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Data.Mapping
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            // Primary Key
            this.HasKey(x => x.ProductId);

            // Properties

            // Table & Column Mappings
            this.ToTable("Products");

            // Navigation properties

            //this.HasMany(t => t.);
        }
    }
}
