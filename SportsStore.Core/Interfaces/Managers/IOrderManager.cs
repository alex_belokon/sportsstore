﻿using SportsStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Core.Interfaces.Managers
{
	public interface IOrderManager
	{
		void ProcessOrder(Cart cart, ShippingDetails shippingDetails);
	}
}
