﻿using SportsStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Core.Interfaces.Managers
{
    public interface IProductManager
    {
        Product[] GetAllProducts();

        Product GetProductById(int productId);

		void AddProduct(string name, string description, string category, decimal price, string imageMimeType, byte[] imageData);

		void EditProduct(int productId, string name, string description, string category, decimal price, string imageMimeType, byte[] imageData);

		void DeleteProduct(int productId);
	}
}
