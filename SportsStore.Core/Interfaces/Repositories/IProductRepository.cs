﻿using SportsStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Core.Interfaces.Repositories
{
    public interface IProductRepository : IRepositoryBase<Product>
    {
        Product Get(int id);
    }
}
