﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Core.Enums
{
    public enum TransactionIsolationLevel
    {
        /// <summary>
        /// A dirty read is possible, meaning that no shared locks are issued
        /// and no exclusive locks are honored.
        /// </summary>
        ReadUncommitted = 0,

        /// <summary>
        /// Shared locks are held while the data is being read to avoid dirty reads,
        ///  but the data can be changed before the end of the transaction, resulting
        ///  in non-repeatable reads or phantom data.
        /// </summary>
        ReadCommitted = 1,

        /// <summary>
        /// Locks are placed on all data that is used in a query, preventing other users
        /// from updating the data. Prevents non-repeatable reads but phantom rows are
        /// still possible.
        /// </summary>
        RepeatableRead = 2,

        /// <summary>
        /// A range lock is placed on the dataset, preventing other users
        /// from updating or inserting rows into the dataset until the transaction is
        /// complete.
        /// </summary>
        Serializable = 3,

        /// <summary>
        /// Reduces blocking by storing a version of data that one application can read
        /// while another is modifying the same data. Indicates that from one transaction
        /// you cannot see changes made in other transactions, even if you re-query.
        /// </summary>
        Snapshot = 4,
    }
}
