﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Core.Models
{
	public class Cart
	{
		private List<CartLine> _cartLinesList = new List<CartLine>();

		public void AddItem(Product product, int quantity)
		{
			var cartLine = _cartLinesList.Where(x => x.Product.ProductId == product.ProductId)
				.FirstOrDefault();
			if (cartLine == null)
			{
				_cartLinesList.Add(new CartLine { Product = product, Quantity = quantity });
			}
			else
			{
				cartLine.Quantity += quantity;
			}
		}
		public void RemoveLine(Product product)
		{
			_cartLinesList.RemoveAll(x => x.Product.ProductId == product.ProductId);
		}
		public decimal ComputeTotalValue()
		{
			return _cartLinesList.Sum(x => x.Product.Price * x.Quantity);
		}
		public void Clear()
		{
			_cartLinesList.Clear();
		}
		public IEnumerable<CartLine> Lines
		{
			get { return _cartLinesList; }
		}
		//public IEnumerable<CartLine> Lines => _cartLinesList; //best
	}

	public class CartLine
	{
		public Product Product { get; set; }
		public int Quantity { get; set; }
	}
}
