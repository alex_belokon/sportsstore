﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Core.Models
{
	public class ShippingDetails
	{
		[Required(ErrorMessage = "Please enter a name")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Please first line")]
		public string Line1 { get; set; }
		public string Line2 { get; set; }
		public string Line3 { get; set; }

		[Required(ErrorMessage = "Please enter City")]
		public string City { get; set; }

		[Required(ErrorMessage = "Please enter City")]
		public string State { get; set; }

		public string Zip { get; set; }

		[Required(ErrorMessage = "Please enter Country")]
		public string Country { get; set; }

		public bool GiftWrap { get; set; }
	}
}
