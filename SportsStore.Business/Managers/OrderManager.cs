﻿using SportsStore.Core.Interfaces.Managers;
using SportsStore.Core.Models;
using SportsStore.Data.EmailSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Business.Managers
{
	public class OrderManager : IOrderManager
	{
		private EmailSettings emailSettings;

		public OrderManager(EmailSettings _emailSettings)
		{
			emailSettings = _emailSettings;
		}

		public void ProcessOrder(Cart cart, ShippingDetails shippingDetails)
		{
			using (var smtpClient = new SmtpClient())
			{
				smtpClient.EnableSsl = emailSettings.UseSsl;
				smtpClient.Host = emailSettings.ServerName;
				smtpClient.Port = emailSettings.ServerPort;
				smtpClient.UseDefaultCredentials = false;
				smtpClient.Credentials = new NetworkCredential(emailSettings.Username, emailSettings.Password);
				if (emailSettings.WriteAsFile)
				{
					smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
					smtpClient.PickupDirectoryLocation = emailSettings.FileLocation;
					smtpClient.EnableSsl = false;
				}
				var mailBody = new StringBuilder();
				mailBody.AppendLine("A new order has been submitted")
				.AppendLine("------------------------------")
				.AppendLine("Items:");
				foreach (var line in cart.Lines)
				{
					var subtotal = line.Product.Price * line.Quantity;
					mailBody.AppendLine(line.Quantity + " x " + line.Product.Name +
										$" (subtotal: {line.Product.Name} : {subtotal}")
						.AppendLine($"Total order value: {cart.ComputeTotalValue()}")
						.AppendLine("--------------------------")
						.AppendLine("Ship to:")
						.AppendLine(shippingDetails.Name)
						.AppendLine(shippingDetails.Line1)
						.AppendLine(shippingDetails.Line2 ?? "")
						.AppendLine(shippingDetails.Line3 ?? "")
						.AppendLine(shippingDetails.City)
						.AppendLine(shippingDetails.State ?? "")
						.AppendLine(shippingDetails.Country)
						.AppendLine(shippingDetails.Zip)
						.AppendLine("--------------------------")
						.AppendLine("Gift wrap : " + (shippingDetails.GiftWrap ? "Yes" : "No"));
				}
				var mailMessage = new MailMessage(
					emailSettings.MailFromAddress,
					emailSettings.MailToAddress,
					"New order submitted!",
					mailBody.ToString()
				);
				if (emailSettings.WriteAsFile)
				{
					mailMessage.BodyEncoding = Encoding.ASCII;
				}
				smtpClient.Send(mailMessage);
			}
		}
	}
}
