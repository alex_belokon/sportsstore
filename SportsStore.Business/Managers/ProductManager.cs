﻿using SportsStore.Core.Interfaces.Managers;
using SportsStore.Core.Interfaces.Repositories;
using SportsStore.Core.Interfaces.UnitOfWork;
using SportsStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Business.Managers
{
    public class ProductManager : IProductManager
    {
        private readonly IProductRepository _productRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductManager(IUnitOfWork unitOfWork, IProductRepository productRepository)
        {
            _unitOfWork = unitOfWork;
            _productRepository = productRepository;
        }

        public Product[] GetAllProducts()
        {
            return _productRepository.SelectAll().ToArray();
        }

        public Product GetProductById(int productId)
        {
            return _productRepository.Find(productId);
        }

		public void AddProduct(string name, string description, string category, decimal price, string imageMimeType, byte[] imageData)
		{
			var product = new Product()
			{
				Name = name,
				Category = category,
				Description = description,
				Price = price,
				ImageData = imageData,
				ImageMimeType = imageMimeType
			};
			_productRepository.Insert(product);
			//(_unitOfWork.DbContext as SportsStoreDbContext).Entry(product).State = product.ProductId == 0 ? EntityState.Added : EntityState.Modified;
			_unitOfWork.Save();
		}

		public void EditProduct(int productId, string name, string description, string category, decimal price, string imageMimeType, byte[] imageData)
		{
			var record = _productRepository.Get(productId);
			if (record != null)
			{
				record.Category = category;
				record.Description = description;
				record.Price = price;
				record.Name = name;
				record.ImageData = imageData;
				record.ImageMimeType = imageMimeType;
				_unitOfWork.Save();
			}
		}

		public void DeleteProduct(int productId)
		{
			var record = _productRepository.Get(productId);
			if (record != null)
			{
				_productRepository.Delete(record);
				_unitOfWork.Save();
			}
		}
	}
}
